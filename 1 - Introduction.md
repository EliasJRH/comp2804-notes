# 1 - Introduction

**Pseudo-theorem**: Among any group of 6 people there are 3 mutual friends or 3 mutual strangers.

We know that: For any two people $`p`$ and $`q`$, $`p\neq q`$

- $`p`$ and $`q`$ are friends OR
- $`p`$ and $`q`$ are not friends

Since this theorem implies a connection or lack thereof between any two people in a group, we can model this relation using a graph.

![](https://i.imgur.com/AlXHqeX.png)

**Rewritten real theorem:** Let $`G`$ be a complete graph on $`6`$ vertices whose edges are colored red and blue. Then $`G`$ contains a monochromatic 3-cycle

_Monochromatic_: All edges are the same color
_3-Cycle_: A cycle containing 3 nodes in a graph

**Proof**:

Take any vertex $`V`$. $`V`$ has exactly 5 incident edges, each colored either red or blue

$`R+B=5`$

Where $`R`$ is the number of red incident edges to $`V`$ and $`B`$ is the number of blue incident edges to $`V`$

- Claim: $`R>2`$ or $`B>2`$
- Short proof by cont: if $`R\leq 2`$ and $`B \leq 2`$ then $`R+B\leq2+2=4<5`$ which contradicts the claim that $`R+B=5`$
- Related: _The Pigeonhole Principle_

Without loss of generality, assume $`R>2`$

Since $`R`$ is an integer, if $`R>2`$ then $`R\geq3`$

Let $`X,Y,Z`$ be vertices such that $`VX,VY,VZ`$ are red.

2 cases to consider

1. $`XYZ`$ has 3 blue edges. In this case, our theorem is true because we have 3 blue edges
2. At least one of $`XY,YZ,ZX`$ is red. Without loss of generality, if $`XY`$ is red, then our theorem is true because $`VXY`$ is a red cycle.

$`\blacksquare Q.E.D`$

_Ramsey Theory:_ A branch of mathematics that focuses on the appearance of order in a substructure given a structure of a known size.

## Quicksort

- Want to sort an array from $`a_1...a_n`$
- If $`n\leq1`$ then do nothing `return a`
- Else, compare evertyhing to $`a_1`$ to partition into a modified array which looks like this.
- ![](https://i.imgur.com/h0CZFST.png)

- Requires $`n-1`$ comparisons
- Recursivly sort x and y in a similar way
- Could be very slow if array is already sorted or close to sorted
- Number of comparisons made on a sorted or near sorted array is $`\approx(n(n-1))/2`$
- We can reduce to roughly $`1.38*nlog_2n`$ by picking a random index to partition the array around

## Sperners theorem

**Question**

Given a set $`S`$ how many subsets $`S_1...S_k`$ can we make such that for any $`i,j`$

- $`i\neq j`$
- $`S_i \nsubseteq S_j`$
- $`S_j \nsubseteq S_i`$

###### tags: `COMP2804`
