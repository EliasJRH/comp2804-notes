# 13 - The O'Reiley Triplets Problem, Conditional Probability, Anil's kids

## The O'Reiley triplets problem

Three good tennis players, all triplets
$`\{\text{Christine, Patti, Terri}\}`$

Petti was the best of the three.

If Petti plays either of her sisters, then she wins.

We don't know which is which but each are wearing different colored clothing. We have enough time to have two of them play a game.

We pick two at random, somebody wins and somebody loses.

We could toss a random coin and pick based of that and be right 50% of the time

Define a probability space

$`S=\{Christine, Patti, Terri\}, \  Pr(\omega)=\frac{1}{3} \text{ for each } w\in S`$
$`
$`A = \text{"The winner of the match is Patti"}`$
$\bar{A} = \text{"The winner of the match is not Patti"}`$

What we choose is the sister we have not play in the match

$`\bar{A} = \{Patti\} A=\{Christine, Terri\}`$

If the winner of the match is not Patti, then the one we picked has to be Patti.
If the winner of the match is Patti, then we picked either Christine or Terri.
Picking Christine or Terry is more likely than picking Patti, so after picking it's better to pick the person who won.

$`Pr(\bar{A}) = \frac{1}{3}`$
$`Pr(A) = 1 - Pr(\bar{A}) = \frac{2}{3}`$

Related [Monty Hall Problem](https://en.wikipedia.org/wiki/Monty_Hall_problem)

## Anil's kids

- Anil has two kids (first born, second born)
- At least one of Anil's children is a boy
- What is the probability that both of Anil's children are boys

Assumptions:

- Each of Anil's children are either a boy or a girl
- It is equally likely for a child to be a boy or a girl
- The event of having a single child is independent

Write down the probability space

$`S = \{BB, BG, GB, GG\} \ Pr(\omega) = \frac{1}{4}`$
$`
$Pr(BB) = \frac{1}{4}`$

We know that at least one of them is a boy so $`GG`$ is not an outcome. Now our probability space becomes

$`S = \{BB, BG, GB\} \ Pr(\omega) = \frac{1}{3}`$

Use the conditional probability equation

$`A = \text{"Anil has two boys"} = \{BB\}`$
$`B = \text{"At least one of Anil's children is a boy"} = \{BB, GB, BG\}`$
$`A \cap B = \{BB\}`$
$`
$Pr(A | B) = \frac{Pr(A \cap B)}{Pr(b)} = \frac{1/4}{3/4} = \frac{1}{3}`$

---

Now we see Anil walking in the mall, and he says his son was born on a Sunday.

What is the probability that both of Anil's kids are boys given that one of his kids is a boy born on a Sunday

New probability space

$`S = \{(s_1, d_1, s_2, d_2)\} \ s_1, s_2 = \{B,G\} \ d_1, d_2 = \{Su, Mo, Tu, Wed, Th, Fri, Sa\}`$
$`
$`|S| = 14^2`$
$` $`Pr(w) = \frac{1}{|S|} = \frac{1}{14^2}`$ $`
$`A = \text{Anil has two boys born on anyday} = \{B, d_1, B, d_2\}, d_1, d_2 \in {\text{Days of the week}}`$
$`B = \text{Anil has one boy born on a sund
$`\cup \{s_1, d_1, B, Su\}, s_2 \in \{B,G\}, d_2 \in \text{Days of the week}`$
$A \cap B = \text{"Anil has two boys at least one of which was born on Sunday"} = \{B, Su, B, d_2\} : d_2 \in \{W\}`$ $\cup \{B, d_1, B, Su\} : d_2 \in \{W\}`$

$Pr(A | B) = \frac{Pr(A \cap B)}{Pr(B)}`$

Call the first part of $`B`$ $`X`$ and the second part $`Y`$ then $`|X| = |Y| = 14`$

$`|B| = |X \cup Y| = |X| + |Y| - |X \cap Y|`$

The only thing that appears in both set is when there are two boys both born on sunday

$`=14 + 14 - 1 = 27`$

Call the two parts of $`A \cap B$: $`P`$ and $`Q`$

$`|P| = |Q| = 7`$ Because the only thing that changes is the day where the second boy is born, and there are 7 options.
$`
$`|A \cap B| = |P \cup Q| = |P| + |Q| - |P \cap Q| = 7 + 7 -1 = 13`$

$Pr(A | B) = \frac{Pr(A \cap B)}{Pr(B)} = \frac{13/14^2}{27/14^2} = \frac{13}{27}`$

The more details given about the child, the more and more likely that that $`Pr(A | B)`$ is 0.50

## Conditional Probability

Used for formalize and tackle problems like the monty hall problem

**Definition: ** For any events $`A`$ and $`B`$ with $`Pr(B) > 0`$
$`Pr(A | B) = \frac{Pr(A \cap B)}{Pr(B)}`$

A given B is equal to the probability of A and B over the probability of B.

## Dice example

Roll a D6
$`S = \{1,2,3,4,5,6\} Pr(w)=\frac{1}{|S|} = frac{1}{6}`$
$`A = \text{"A three is rolled"} = \{3\}`$
$`B = \text{"An odd integer is rolled"} = \{1,3,5\}`$
$`
$Pr(A | B) = \frac{Pr(A \cap B)}{Pr(B)} = \frac{Pr(\{3\})}{Pr(\{1,3,5\})} = \frac{1/6}{3/6} = \frac{1}{3}`$

When the probabilites are uniform we can have that
$`\frac{Pr(A \cap B)}{Pr(B)} = \frac{|A \cap B| / |S|}{|B| / |S|}`$

Same scenario

$`C = \text{"A prime number is rolled"} = \{2,3,5\}`$
$`
$`Pr(A | C) = \frac{Pr(A \cap C)}{Pr(C)} = \fr $`
$`Pr(B | C) = \frac{2/3}{3/6} = \frac{2}{3}`$

$\bar{C} = \{1,4,6\}`$

$Pr(B | \bar{C}) = \frac{Pr(B \cap C)}{Pr(\bar{C})} = \frac{Pr(\{1\})}{3/6} = \frac{1}{3}`$

###### tags: `COMP2804` `Probability`
