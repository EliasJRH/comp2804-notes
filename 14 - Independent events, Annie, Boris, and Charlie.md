# 14 - Independent events, Annie, Boris, and Charlie

## Independent events

**Definition:** Two events $`A`$ and $`B`$ are **independent** if $`Pr(A \cap B) = Pr(A) * Pr(B)`$

**Consequence:** If $`A`$ and $`B`$ are independent and $`Pr(B) > 0`$ then $`Pr(A | B) = \frac{Pr(A\cap B)}{Pr(B)} = \frac{Pr(A)Pr(B)}{Pr(B)} = Pr(A)`$

Ex: Rolling two dice

$`S = \{(D_1, D_2): D_1, D_2 \in \{1,2,3,4,5,6\}\}`$ $`Pr(\omega ) = \frac{1}{|S|} = \frac{1}{36}`$
$`
$`A = D_2 = 6 = \{(1,6),(2,6),(3,6),(4,6),(5,6),(6,6)\}`$
$`B = D_1 = 4 = \{(4,1),(4,2),(4,3),(4,4),(4,5),(4,6)\}`
$`
$Pr(A) = \frac{|A|}{|S|} = \frac{6}{36} = \frac{1}{6}`$ $Pr(B) = \frac{|B|}{|S|} = \frac{6}{36} = \frac{1}{6}`$

Another ex:

$`A = D_1+D_2=7 = \{(1,6),(2,5),(3,4),(4,3),(5,2),(6,1)\}`$
$`B = D_1 =4 = \{(4,1),(4,2),(4,3),(4,4),(4,5),(4,6)\}`$
$`
$`Pr(A) = \frac{|A|}{|S|} = \frac{6}{36} = \frac{1}{6}`$
$`Pr(B) = \frac{|B|}{|S|} = \frac{6}{36} = \frac{1}{6}`$
$` $Pr(A \cap B) = Pr(\{(4,3)\}) = \frac{1}{36} = \frac{1}{6} * \frac{1}{6} = Pr(A) \times Pr(B)`$

$\therefore`$ A and B are independent!

### Base property

If $`A`$ and $`B`$ are independent then $`A`$ and $`\bar{B}`$ are independent

To prove this we need to show that $`Pr(A \cap \bar{B}) = Pr(A) \times Pr(\bar{B}) = Pr(A) \times (1 - Pr(B))`$

Because $`Pr(A \cap \bar{B})`$ and $`Pr(A \cap B)`$ are disjoint, then
$`Pr(A) = Pr(A \cap \bar{B}) + Pr(A \cap B)`$
$`Pr(A) = Pr(A) * Pr(B) + Pr(A \cap \bar{B})`$
$`Pr(A \cap \bar{B}) = Pr(A) - Pr(A) * Pr(B)`$
$`= Pr(A) * (1 - Pr(B)) = Pr(A) * Pr(\bar{B})`$

## Multi independence

Pairwise independent: A sequence of events $`A_1, A_2,..., A_n`$ is **Pairwise independance** if for any $`1 \leq i \leq j \leq n`$, $`Pr(A_i \cap A_j) = Pr(A_i) \times Pr(A_j)`$
In other words, $`A_i`$ and $`A_j`$ are independent

Mutually independent: A sequence of events $`A_1, A_2,..., A_n`$ is **mutually independent** if for each $`k \in \{2,3,4,...,n\}`$ and each $`1 \leq i_1 \leq i_2 \leq ... \leq i_k \leq n`$ (for each subset of events of size $`\geq 2`$)
$`Pr(A_{i_1} \cap A_{i_2} \cap ... \cap A_{i_n}) = Pr(A_{i_1}) * Pr(A_{i_2}) * ... * Pr(A_{i_n})`$

Example where these two are different:

$`S = \{00, 01, 10, 11\}`$ $`Pr(\omega ) = \frac{1}{4}`$ Bitstring $`b_1, b_2`$
$`
$`A = b_1 = 1 = \{10, 11\}`$
$`B = b_2 = 1 = \{01, 11\}`$
$`C = b_1 \neq b_2 = \{10, 01\}`$

$Pr(A) = \frac{2}{4} = \frac{1}{2} = Pr(B) = Pr(C)`$

Check for pairwise independence
$`Pr(A \cap B) = Pr(\{11\}) = \frac{1}{4} = \frac{1}{2} \times \frac{1}{2} = Pr(A) \times Pr(B)`$
$`Pr(A \cap C) = Pr(\{10\}) = \frac{1}{4} = \frac{1}{2} \times \frac{1}{2} = Pr(A) \times Pr(C)`$
$`Pr(B \cap C) = Pr(\{01\}) = \frac{1}{4} = \frac{1}{2} \times \frac{1}{2} = Pr(B) \times Pr(C)`$

Therefore $`A, B`$ and $`C`$ are pairwise independent.

Now to check for mutual independence
$`Pr(A \cap B \cap C) = Pr(\{\emptyset \}) = 0 \neq \frac{1}{8} = Pr(A) \times Pr(B) \times Pr(C)`$

Therefore $`A, B`$ and $`C`$ are not mutually independent.

## 3 Students

Alina, Bart and Charlie
3 Students, each student writes a 1-question all or nothing exam.
$`Pr(\text{Alina answers correctly}) = Pr(A) = \frac{9}{10}`$
$`Pr(B) = \frac{9}{10}`$
$`Pr(C) = \frac{6}{10}`$
$`
$`A, B, C`$ are mutually independent.
$`
$`E = \text{At least 2 students pass / answer correctly} = (A \cap B) \cup (B \cap C) \cu $`Pr(E) = Pr(A \cap B) + Pr(B \cap C) + Pr(A \cap C) - 2Pr(A \cap B \cap C)`$ $Pr(E) = (Pr(A) * Pr(B)) + (Pr(C) * Pr(B)) + (Pr(A) * Pr(C)) - 2(Pr(A) * Pr(B)* Pr(C))`$
$= (\frac{9}{10} _ \frac{9}{10}) + (\frac{9}{10} _ \frac{6}{10}) + (\frac{9}{10} _ \frac{6}{10}) - 2(\frac{9}{10} _ \frac{9}{10} \* \frac{6}{10}) = \frac{459}{500} = 0.918`$

**Second scenario**
Charlie copies Barts answer, now 2 students pass if Bart passes

$`E = B`$
$`E = Pr(B) = \frac{9}{10} = 0.90`$

###### tags: `COMP2804`, `Probability`
